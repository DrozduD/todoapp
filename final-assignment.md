## Finałowe zadanie

Zadanie do wykonania polega na napisaniu testów jednostkowych wraz z implementacją nowej funkcjonalności:

- usuwania todo item przez kliknięcie w ikonę kosza przypisaną do danego todo
- ukrywanie wszystkich todo item które zostały ukończone (completed jest `true`)
- pokazanie wiadomości gdy wszystkie todo items zostały ukończone

Ostateczna funkcjonalność prezentowana jest w filmiku `Final assignment.mp4`.

Przygotowana jest gałąź `final-assignment` gdzie do listy items dołączony jest element `svg` który przedstawia ikonę kubła na śmieci :)

Proszę zrobić `fork` repozytorium na swoje konto w Gitlab, jako `private` i tam robić commits, a po zakończeniu proszę przesłać mi wiadomość na discord z linkiem do repozytorium.

Pytania proszę pisać w wątku do testów i tagoować mnie @Qmor

Powodzenia!
