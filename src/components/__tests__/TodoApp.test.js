import { screen, render, within } from '@testing-library/svelte';
import userEvent from '@testing-library/user-event';

import TodoApp from '../TodoApp.svelte';

test('should render with default values', () => {
  // 1. Setup
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  // screen.getByRole('heading', { name: /Todo App/, level: 1 }))
  // equals to
  expect(screen.queryByRole('heading', { name: /Todo App/, level: 1 })).toBeInTheDocument();

  const list = screen.getByRole('list');

  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
  within(list).getByText(/No todos found 😱/);

  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);

  expect(input).toHaveValue('');

  screen.getByRole('button', { name: /Add todo/ });
});

test('can add todo item', async () => {
  // 1. Setup
  const user = userEvent.setup();
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  const list = screen.getByRole('list');

  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
  within(list).getByText(/No todos found 😱/);

  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);

  expect(input).toHaveValue('');

  const button = screen.getByRole('button', { name: /Add todo/ });

  const todoText = 'Finish this tutorial!';

  await user.type(input, todoText);
  await user.click(button);

  expect(input).toHaveValue('');

  expect(within(list).queryByRole('listitem')).toBeInTheDocument();
  expect(within(list).queryByText(/No todos found 😱/)).not.toBeInTheDocument();

  const firstTodo = within(list).queryByRole('listitem', { name: todoText });

  expect(firstTodo).toHaveTextContent(todoText);
  expect(within(firstTodo).getByRole('checkbox')).toHaveProperty('checked', false);
  expect(completedHeading).toHaveTextContent('0/1');
});

test('can toggle completed state of a todo item', async () => {
  // 1. Setup
  const user = userEvent.setup();
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  const list = screen.getByRole('list');
  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
  within(list).getByText(/No todos found 😱/);

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);

  expect(input).toHaveValue('');

  const button = screen.getByRole('button', { name: /Add todo/ });

  const todoText = 'Finish this tutorial!';

  await user.type(input, todoText);
  await user.click(button);

  expect(input).toHaveValue('');

  expect(within(list).queryByRole('listitem')).toBeInTheDocument();
  expect(within(list).queryByText(/No todos found 😱/)).not.toBeInTheDocument();

  const firstTodo = within(list).queryByRole('listitem', { name: todoText });
  const firstTodoCheckbox = within(firstTodo).getByRole('checkbox', { checked: false });

  expect(firstTodo).toHaveTextContent(todoText);
  expect(within(firstTodo).getByRole('checkbox')).toHaveProperty('checked', false);
  expect(completedHeading).toHaveTextContent('0/1');

  await user.click(firstTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('1/1');

  await user.click(firstTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', false);
  expect(completedHeading).toHaveTextContent('0/1');

  const todoText2 = 'Create another tutorial!';

  await user.type(input, todoText2);
  await user.click(button);

  const todoItems = within(list).queryAllByRole('listitem');
  const secondTodo = within(list).queryByRole('listitem', { name: todoText2 });
  const secondTodoCheckbox = within(secondTodo).getByRole('checkbox', { checked: false });

  expect(within(secondTodo).getByRole('checkbox')).toHaveProperty('checked', false);

  expect(todoItems).toHaveLength(2);
  expect(completedHeading).toHaveTextContent('0/2');

  await user.click(secondTodoCheckbox);

  expect(secondTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('1/2');

  await user.click(firstTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', true);
  expect(secondTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('2/2');
});

test('can delete todo item from the list', async () => {
  // 1. Setup
  const user = userEvent.setup();
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  const list = screen.getByRole('list');
  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
  within(list).getByText(/No todos found 😱/);

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);

  expect(input).toHaveValue('');

  const button = screen.getByRole('button', { name: /Add todo/ });

  const todoText = 'Finish this tutorial!';

  await user.type(input, todoText);
  await user.click(button);

  expect(input).toHaveValue('');
  expect(within(list).queryByRole('listitem')).toBeInTheDocument();
  expect(within(list).queryByText(/No todos found 😱/)).not.toBeInTheDocument();

  const todoText2 = 'Create another tutorial!';

  await user.type(input, todoText2);
  await user.click(button);

  expect(input).toHaveValue('');

  var todoItems = within(list).queryAllByRole('listitem');
  expect(todoItems).toHaveLength(2);

  const firstTodo = within(list).queryByRole('listitem', { name: todoText });
  const firstTodoDeleteButton = await within(firstTodo).getByRole('button');

  expect(firstTodo).toHaveTextContent(todoText);
  expect(completedHeading).toHaveTextContent('0/2');

  await user.click(firstTodoDeleteButton);
  expect(completedHeading).toHaveTextContent('0/1');
  todoItems = within(list).queryAllByRole('listitem');
  expect(todoItems).toHaveLength(1);

  const secondTodo = within(list).queryByRole('listitem', { name: todoText2 });
  expect(secondTodo).toHaveTextContent(todoText2);

  const secondTodoDeleteButton = await within(secondTodo).getByRole('button');
  await user.click(secondTodoDeleteButton);
  todoItems = within(list).queryAllByRole('listitem');
  expect(todoItems).toHaveLength(0);
  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
});

test('can hide completed todo tasks', async () => {
  // 1. Setup
  const user = userEvent.setup();
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  const list = screen.getByRole('list');
  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(within(list).queryByRole('listitem')).not.toBeInTheDocument();
  within(list).getByText(/No todos found 😱/);

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);

  expect(input).toHaveValue('');

  const button = screen.getByRole('button', { name: /Add todo/ });

  const todoText = 'Finish this tutorial!';

  await user.type(input, todoText);
  await user.click(button);

  expect(input).toHaveValue('');

  expect(within(list).queryByRole('listitem')).toBeInTheDocument();
  expect(within(list).queryByText(/No todos found 😱/)).not.toBeInTheDocument();

  const firstTodo = within(list).queryByRole('listitem', { name: todoText });
  const firstTodoCheckbox = within(firstTodo).getByRole('checkbox', { checked: false });

  expect(firstTodo).toHaveTextContent(todoText);
  expect(within(firstTodo).getByRole('checkbox')).toHaveProperty('checked', false);
  expect(completedHeading).toHaveTextContent('0/1');

  await user.click(firstTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('1/1');

  const todoText2 = 'Create another tutorial!';

  await user.type(input, todoText2);
  await user.click(button);

  let todoItems = within(list).queryAllByRole('listitem');
  const secondTodo = within(list).queryByRole('listitem', { name: todoText2 });

  const hideCheckbox = screen.getByRole('checkbox', { name: 'Hide completed' });

  expect(within(secondTodo).getByRole('checkbox')).toHaveProperty('checked', false);

  expect(todoItems).toHaveLength(2);
  expect(completedHeading).toHaveTextContent('1/2');

  await user.click(hideCheckbox);

  expect(hideCheckbox).toHaveProperty('checked', true);
  todoItems = within(list).queryAllByRole('listitem');
  expect(todoItems).toHaveLength(1);
  expect(completedHeading).toHaveTextContent('1/2');

  await user.click(hideCheckbox);

  expect(hideCheckbox).toHaveProperty('checked', false);
  todoItems = within(list).queryAllByRole('listitem');
  expect(todoItems).toHaveLength(2);
  expect(completedHeading).toHaveTextContent('1/2');
});

test('show message when all tasks are done', async () => {
  // 1. Setup
  const user = userEvent.setup();
  // 2. Render
  render(TodoApp);
  // 3. Expectations
  const list = screen.getByRole('list');
  const completedHeading = screen.queryByRole('heading', { name: /Completed:/, level: 3 });

  expect(completedHeading).toBeInTheDocument();
  expect(completedHeading).toHaveTextContent('0/0');

  const input = screen.getByPlaceholderText(/Tell me what to do.../);
  const button = screen.getByRole('button', { name: /Add todo/ });

  const todoText = 'Finish this tutorial!';

  await user.type(input, todoText);
  await user.click(button);

  const firstTodo = within(list).queryByRole('listitem', { name: todoText });
  const firstTodoCheckbox = within(firstTodo).getByRole('checkbox', { checked: false });

  await user.click(firstTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('1/1');

  const todoText2 = 'Create another tutorial!';

  await user.type(input, todoText2);
  await user.click(button);

  const secondTodo = within(list).queryByRole('listitem', { name: todoText2 });
  const secondTodoCheckbox = within(secondTodo).getByRole('checkbox', { checked: false });

  await user.click(secondTodoCheckbox);

  expect(firstTodoCheckbox).toHaveProperty('checked', true);
  expect(secondTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('2/2');

  const doneMessage = screen.getByText(/Great success! You have completed all items!/);
  expect(doneMessage).toBeInTheDocument();

  const emoji = within(doneMessage).getByTitle('emoji')
  expect(emoji).toBeInTheDocument();

  expect(secondTodoCheckbox).toHaveProperty('checked', true);
  expect(completedHeading).toHaveTextContent('2/2');
});
